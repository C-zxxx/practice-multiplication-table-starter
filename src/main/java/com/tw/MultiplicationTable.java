package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if(isValid(start, end)){
           return generateTable(start, end);
        }
        return null;
    }

    public Boolean isValid(int start, int end) {
        return isInRange(start) && isInRange(end) && isStartNotBiggerThanEnd(start, end);
    }

    public Boolean isInRange(int number) {
        return number >= 1 && number <= 1000;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        String table = "";
        for (int i = start; i < end; i++) {
            table += generateLine(start, i);
            table += "%n";
        }
        table += generateLine(start, end);
        return String.format(table);
    }

    public String generateLine(int start, int row) {
        String line = "";
        for (int i = start; i < row; i++) {
            line += generateSingleExpression(i, row);
            line += "  ";
        }
        line += generateSingleExpression(row, row);
        return line;
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
//        return String.format("%d*%d=%d",multiplicand,multiplier,multiplicand*multiplier);
//        return multiplicand + "*" + multiplier + "=" + (multiplicand*multiplier) ;
        StringBuilder singleExpression = new StringBuilder();
        singleExpression = singleExpression.append(multiplicand).append("*").append(multiplier).append("=").append(multiplicand*multiplier);
        return singleExpression.toString();
    }
}
